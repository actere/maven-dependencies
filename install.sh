#!/bin/sh
cwd=$(pwd)

/cygdrive/c/Users/michael.kunz/Documents/development/tools/apache-maven-3.6.0/bin/mvn org.apache.maven.plugins:maven-install-plugin:2.5.2:install-file -DupdateReleaseInfo=true -Dfile="${1}" -DpomFile="${1}" -Dpackaging=pom -DcreateChecksum=true "-DlocalRepositoryPath=$cwd"

for f in ${1/%.pom/*.jar}
do
	[ -e "$f" ] || continue
	case "$f" in
	*-sources.jar)
		/cygdrive/c/Users/michael.kunz/Documents/development/tools/apache-maven-3.6.0/bin/mvn org.apache.maven.plugins:maven-install-plugin:2.5.2:install-file -DupdateReleaseInfo=true -Dfile="${f}" -Dpackaging=jar -Dclassifier=sources -DpomFile="${1}" -DcreateChecksum=true "-DlocalRepositoryPath=$cwd"
		;;
	*-javadoc.jar)
		/cygdrive/c/Users/michael.kunz/Documents/development/tools/apache-maven-3.6.0/bin/mvn org.apache.maven.plugins:maven-install-plugin:2.5.2:install-file -DupdateReleaseInfo=true -Dfile="${f}" -Dpackaging=jar -Dclassifier=javadoc -DpomFile="${1}" -DcreateChecksum=true "-DlocalRepositoryPath=$cwd"
		;;
	*-tests.jar)
		/cygdrive/c/Users/michael.kunz/Documents/development/tools/apache-maven-3.6.0/bin/mvn org.apache.maven.plugins:maven-install-plugin:2.5.2:install-file -DupdateReleaseInfo=true -Dfile="${f}" -Dpackaging=jar -Dclassifier=tests -DpomFile="${1}" -DcreateChecksum=true "-DlocalRepositoryPath=$cwd"
		;;
	*-javafx.jar)
		/cygdrive/c/Users/michael.kunz/Documents/development/tools/apache-maven-3.6.0/bin/mvn org.apache.maven.plugins:maven-install-plugin:2.5.2:install-file -DupdateReleaseInfo=true -Dfile="${f}" -Dpackaging=jar -Dclassifier=javafx -DpomFile="${1}" -DcreateChecksum=true "-DlocalRepositoryPath=$cwd"
		;;
	*)
		/cygdrive/c/Users/michael.kunz/Documents/development/tools/apache-maven-3.6.0/bin/mvn org.apache.maven.plugins:maven-install-plugin:2.5.2:install-file -DupdateReleaseInfo=true -Dfile="${f}" -Dpackaging=jar -DpomFile="${1}" -DcreateChecksum=true "-DlocalRepositoryPath=$cwd"
		;;
	esac
done

for f in ${1/%.pom/*.zip}
do
	[ -e "$f" ] || continue
	/cygdrive/c/Users/michael.kunz/Documents/development/tools/apache-maven-3.6.0/bin/mvn org.apache.maven.plugins:maven-install-plugin:2.5.2:install-file -DupdateReleaseInfo=true -Dfile="${f}" -Dpackaging=zip -DpomFile="${1}" -DcreateChecksum=true "-DlocalRepositoryPath=$cwd"
done

#find . -iname "*.pom" -print0 | xargs -0 -n1 -P8 ./install.sh